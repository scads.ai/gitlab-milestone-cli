#!/usr/bin/env bash
shell_found=
for the_shell in bash zsh
do
if [[ $SHELL =~ $the_shell ]]
then
shell_found="yes"
echo "shell detected: $the_shell"
break;
fi
done
if [ -z $shell_found ]
then
echo "Only bash and zsh supported, you are using a different shell"
exit 1
fi

install_dir=$HOME/.jans-gitlab-milestone-cli
some_prerequisite_missing=0

function checkCommandAvailable(){
command_to_check=$1
if ! which $command_to_check > /dev/null
then
echo "Command $command_to_check not found"
some_prerequisite_missing=1
fi
}

echo "checking prerequisites..."
checkCommandAvailable git
checkCommandAvailable curl
checkCommandAvailable jq
checkCommandAvailable sed

if [ $some_prerequisite_missing -ne 0 ]
then
echo "Some prerequisite missing, please install!"
exit 1
fi

if [ '!' -e $install_dir/my_read_api_token ]; then
cat <<-EOF
Please go to https://gitlab.hrz.tu-chemnitz.de/-/profile/personal_access_tokens and create an access token. It is required to read milestones from private projects and also for confidential issues.
These information might help you:
Token name: gitlab-milestone-cli-script
Scope: read_api
If you select api instead of read_api, you can also use the script take-issue later.
If you click on "Create personal access token", you are given the token.
The token is going to be stored at $install_dir/my_read_api_token. Please paste it after the prompt.
EOF
read -p "Token: " TOKEN

echo "installing to: $install_dir/"
mkdir -p "$install_dir"
echo $TOKEN > $install_dir/my_read_api_token
else
echo "installing to: $install_dir/"
mkdir -p "$install_dir"
fi

if [ -e $install_dir/my_username ]; then
GITLAB_USERNAME=`cat $install_dir/my_username`
echo "Your username seems to be $GITLAB_USERNAME. Found in $install_dir/my_username."
else
GITLAB_USERNAME=`curl -s --header "Content-Type: application/json" --header "Authorization: Bearer $TOKEN" -X POST -d "{ \"query\": \"query{currentUser{username}}\"}" "https://gitlab.hrz.tu-chemnitz.de/api/graphql" | jq -r '.data.currentUser.username'`
echo "$GITLAB_USERNAME" > $install_dir/my_username
echo "Your username seems to be $GITLAB_USERNAME. Stored in $install_dir/my_username."
fi

cat  <<-"EOF" | sed "s#INSTALL_DIR#$install_dir#" >"$install_dir/showmilestones.sh"
#!/bin/bash
BASE_URL="https://gitlab.hrz.tu-chemnitz.de/api/graphql"
TOKEN=`cat INSTALL_DIR/my_read_api_token`

function get_project_name(){
git remote get-url origin | sed 's/git@gitlab.hrz.tu-chemnitz.de:\(.*\).git/\1/'
}

function run_curl(){
PROJECT_NAME=`get_project_name`
curl -s --header "Content-Type: application/json" --header "Authorization: Bearer $TOKEN" -X POST -d "{ \"query\": \"query{project(fullPath:\\\"$PROJECT_NAME\\\"){issues(state:opened){nodes{iid,title,milestone{title}}}mergeRequests(state:opened){nodes{iid,title,milestone{title},headPipeline{detailedStatus{label}}}}}}\"}" "$BASE_URL"
}

run_curl | jq -r '.data.project | reduce ([.issues.nodes[].milestone.title] | unique | .[]) as $item ({"orig":.,"filtered":[]};{"orig":.["orig"],"filtered":(.["filtered"] + [("Milestone: " + ($item // "(none)")," Issues:", (.["orig"].issues.nodes[] | select(.milestone.title == $item) | "\(.iid | tostring | (" " * (5 - length)) + .) - \(.title)"), " MRs:", (.["orig"].mergeRequests.nodes[] | select(.milestone.title == $item) | "\(.iid | tostring | (" " * (5 - length)) + .) - \(.title) (pipeline: \(.headPipeline.detailedStatus.label))"),"")])}) | .["filtered"][]'
EOF
chmod u+x "$install_dir/showmilestones.sh"

cat  <<-"EOF" | sed "s#INSTALL_DIR#$install_dir#" >"$install_dir/showissue.sh"
#!/bin/bash
BASE_URL="https://gitlab.hrz.tu-chemnitz.de/api/graphql"
TOKEN=`cat INSTALL_DIR/my_read_api_token`

function get_project_name(){
git remote get-url origin | sed 's/git@gitlab.hrz.tu-chemnitz.de:\(.*\).git/\1/'
}

function run_curl(){
ISSUE=$1
PROJECT_NAME=`get_project_name`
curl -s --header "Content-Type: application/json" --header "Authorization: Bearer $TOKEN" -X POST -d "{ \"query\": \"query{project(fullPath:\\\"$PROJECT_NAME\\\"){issue(iid:\\\"$ISSUE\\\"){iid,title,author{name},assignees{nodes{name}},milestone{title},description,discussions{nodes{notes{nodes{author{name},system,updatedAt,body}}}}}}}\"}" "$BASE_URL"
}

run_curl "$1" | jq -r '.data.project.issue | ("\(.iid | tostring) - \(.title)\nAuthor: \(.author.name)\nAssignee: \(.assignees.nodes[0].name)\nMilestone: \(.milestone.title)\nDescription: \(.description)\nDiscussion:", (.discussions.nodes[] | .notes.nodes[] | "  \(.author.name) at \(.updatedAt):" + (if .system then "" else "\n" end) +"    \(.body)"))'
EOF
chmod u+x "$install_dir/showissue.sh"

cat  <<-"EOF" | sed "s#INSTALL_DIR#$install_dir#" >"$install_dir/takeissue.sh"
#!/bin/bash
BASE_URL="https://gitlab.hrz.tu-chemnitz.de/api/graphql"
TOKEN=`cat INSTALL_DIR/my_read_api_token`
GITLAB_USERNAME=`cat INSTALL_DIR/my_username`

function get_project_name(){
git remote get-url origin | sed 's/git@gitlab.hrz.tu-chemnitz.de:\(.*\).git/\1/'
}

function run_curl(){
ISSUE=$1
PROJECT_NAME=`get_project_name`
if [ -n "$PROJECT_NAME" -a -n "$GITLAB_USERNAME" ]
then
curl -s --header "Content-Type: application/json" --header "Authorization: Bearer $TOKEN" -X POST -d "{ \"query\": \"mutation{issueSetAssignees(input:{projectPath:\\\"$PROJECT_NAME\\\",iid:\\\"$ISSUE\\\",assigneeUsernames:\\\"$GITLAB_USERNAME\\\"}){issue{iid,assignees{nodes{username}}}}}\"}" "$BASE_URL"
fi
}

run_curl "$1" | jq -r 'if .errors then "Error: \(.errors[0].message)" else .data.issueSetAssignees.issue | "Assignee \(.assignees.nodes[0].username) set for \(.iid)" end'
EOF
chmod u+x "$install_dir/takeissue.sh"

cat <<-"EOF" | sed "s#INSTALL_DIR#$install_dir#" >"$install_dir/${the_shell}-extension"
function show-milestones(){
INSTALL_DIR/showmilestones.sh
}
function show-issue(){
if [ $# -eq 1 ];
then
INSTALL_DIR/showissue.sh $1
else
echo 'issue id required'
fi
}
function take-issue(){
if [ $# -eq 1 ]
then
INSTALL_DIR/takeissue.sh $1
else
echo 'issue id required'
fi
}
EOF

if ! grep "$install_dir/${the_shell}-extension" "$HOME/.${the_shell}rc" > /dev/null
then
echo ". \"$install_dir/${the_shell}-extension\"" >> $HOME/.${the_shell}rc
fi

cat <<-"EOF" | sed "s#THE_SHELL#${the_shell}#"
Everything prepared!
You can use the command `show-milestones` from any cloned repo to get a list of milestones with their issues and MRs. Use `show-issue <id>` to inspect issue <id>. If you created your token with scope `api`, you can also use `take-issue <id>` to assign yourself to issue <id>.
EOF
