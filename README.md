# Gitlab Milestone CLI

This project aims to provide a CLI to get information about milestones, such as open issues and open merge requests, from Gitlab's GraphQL API. It is built to work with gitlab.hrz.tu-chemnitz.de ONLY!
Use `install.sh` for an interactive installation.
